/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2015
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.extensions.medias;

import ninjas.extensions.medias.html.HTMLClient ;
import ninjas.extensions.medias.html.functions.HTMLViewHide ;
import ninjas.extensions.medias.html.functions.HTMLViewLoadURL ;
import ninjas.extensions.medias.html.functions.HTMLViewShow ;

import android.webkit.WebView;
import android.webkit.WebSettings;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.MediaController;
import android.widget.VideoView;

import java.util.HashMap;
import java.util.Map;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;

import ninjas.extensions.medias.HTMLViewLoad ;

public class HTMLViewContext extends FREContext
{
    /**
     * Creates a new HTMLViewContext instance.
     */
    public HTMLViewContext()
    {
         
    }
    
    @Override
    public void dispose()
    {
        _functions = null ;
        _settings  = null ;
        _webview   = null ;
    }
    
    @Override
    public Map<String, FREFunction> getFunctions()
    {
        _functions = new HashMap<String, FREFunction>();
        
        _functions.put( "hide"    , new HTMLViewHide()    );
        _functions.put( "loadURL" , new HTMLViewLoadURL() );
        _functions.put( "show"    , new HTMLViewShow()    );
        
        return _functions;
    }

    /**
     * Returns the container reference.
     */
    public ViewGroup getContainer()
    {
        if (_container == null)
        {
            _container = new FrameLayout( getActivity() );
            _container.addView( getHTMLView() , new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        }
        
        return _container;
    }

    /**
     * Returns the root reference.
     */
    public ViewGroup getRoot()
    {
        return (ViewGroup)( (ViewGroup) getActivity().findViewById( android.R.id.content ) ).getChildAt(0) ;
    }

    /**
     * Returns the video reference.
     */
    public VideoView getHTMLView()
    {
        if( _webview == null )
        {
            _view     = new WebView( getActivity() );
            _settings = _view.getSettings() ;

            _view.setZOrderOnTop( true );
            _view.setWebViewClient( new HTMLClient() );
        }
        
        return _view;
    }

    /////////////////

    /**
     * @private
     */
    private ViewGroup _container = null;

    /**
     * The available functions of this context.
     */
    protected HashMap<String, FREFunction> _functions = null;

    /**
     * @private
     */
    private WebSettings _settings = null;

    /**
     * @private
     */
    private WebView _view = null ;

    /////////////////

}
