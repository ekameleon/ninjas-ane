﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2015
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.medias 
{
    import flash.events.StatusEvent;
    import flash.external.ExtensionContext;
    import flash.system.Capabilities;

    /**
     * This extension to display a native html view.
     */
    public class HTMLView 
    {
        /**
         * Creates a new HTMLView instance.
         */
        public function HTMLView()
        {
            _context = ExtensionContext.createExtensionContext( EXTENSION_ID , null ) ;
        }
        
        /**
         * The id of the extension.
         */
        public static const EXTENSION_ID:String = "ninjas.extensions.medias.HTMLView" ;
        
        /**
         * Indicates if the native extension is supported in the application.
         */
        public function get available():Boolean
        {
            //return ( isIOS || isAndroid ) && ( _context != null ) ;
            return isAndroid && Boolean(_context) ;
        }
        
        /**
         * Dispose the extension.
         */
        public function dispose():void
        {
            _context = null ;
        }
        
        /**
         * Hide the view.
         */
        public function hide() : void
        {
            if (!available) return ;
            
            _context.call( "hide" );
        }
        
        /**
         * Loads the given URL.
         */
        public function loadURL( url:String = "" ):void
        {
            if (!available) return ;

            _context.call( "loadURL" , url ) ;
        }

        /**
         * Resizes and repositions the view.
         */
        public function setLayout( x:uint = 0, y:uint = 0 , width:uint, height:uint ):void
        {
            if (!available) return ;

            _context.call( "setLayout" , x , y , width , height ) ;
        }

        /**
         * Show the view.
         */
        public function show() : void
        {
            if (!available) return ;
            
            _context.call( "show" ) ;
        }
        
        /**
         * @private
         */
        private var _context:ExtensionContext ;

        /**
         * @private
         */
        private static const isAndroid:Boolean = (Capabilities.manufacturer.indexOf("Android") != -1) ;
        
        /**
         * @private
         */
        private static const isIOS:Boolean = (Capabilities.manufacturer.indexOf("iOS") != -1);
        
        /**
         * @private
         */
        private function _status( event:StatusEvent ) : void
        {
            //if ( event.code == "COMPLETE" )
            //{
            //    // finished
            //}
        }
    }
    
}
