﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.camera
{
    /**
     * Information about a camera.
     */
    public class CameraInfo
    {
        /**
         * Creates a new CameraInfo instance.
         * @param facing The facing of the camera is opposite to that of the screen.
         * @param orientation The orientation of the camera image.
         */
        public function CameraInfo( facing:int = 0  , orientation:int = 0 )
        {
            this.facing = facing ;
            this.orientation = orientation ;
        }
        
        /**
         * The facing of the camera is opposite to that of the screen.
         */
        public static const CAMERA_FACING_BACK:int = 0 ;
        
        /**
         * The facing of the camera is the same as that of the screen.
         */
        public static const CAMERA_FACING_FRONT:int = 1 ;
        
        /**
         * The direction that the camera faces. It should be CAMERA_FACING_BACK or CAMERA_FACING_FRONT.
         */
        public var facing:int ;
        
        /**
         * The orientation of the camera image. 
         * The value is the angle that the camera image needs to be rotated clockwise so it shows correctly on the display in its natural orientation. 
         * It should be 0, 90, 180, or 270. For example, suppose a device has a naturally tall screen. 
         * The back-facing camera sensor is mounted in landscape. You are looking at the screen. 
         * If the top side of the camera sensor is aligned with the right edge of the screen in natural orientation, the value should be 90. 
         * If the top side of a front-facing camera sensor is aligned with the right of the screen, the value should be 270.
         */
        public var orientation:int ;
    }
}
