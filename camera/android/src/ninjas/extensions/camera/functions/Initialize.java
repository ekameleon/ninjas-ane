/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.camera.functions;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

public class Initialize implements FREFunction
{
    /**
     * Creates a new Initialize instance.
     */
    public Initialize()
    {
        // 
    }
    
    @Override
    public FREObject call( FREContext context , FREObject[] arguments )
    {
        Log.i( tag , " call" );
        
        //Activity activity = context.getActivity() ;
        
        CameraInfo info = new CameraInfo() ;
        
        int numCameras = Camera.getNumberOfCameras() ;
        
        Camera.getCameraInfo( 0 , info ) ;
        
        //Parameters parameters = camera.getParameters();
        
        Log.d( tag , " ########################### " ) ;
        
        Log.i( tag , " version SDK : " + Integer.parseInt( Build.VERSION.SDK ) ) ;
        
        Log.i( tag , " cameras             : " + numCameras ) ;
        Log.i( tag , " cameras orientation : " + info.orientation ) ;
        Log.i( tag , " cameras facing back : " + ( info.facing == CameraInfo.CAMERA_FACING_BACK ) ) ;
        
        Camera camera = Camera.open() ;
        
        Log.i( tag , " camera      : " + camera ) ;
        
        Parameters parameters = camera.getParameters() ;
        
        Log.i( tag , " parameters  : " + parameters ) ;
        
        Log.i( tag , " scene mode  : " + parameters.getSceneMode() ) ;
//        
//        parameters.set("orientation", "portrait");
//        parameters.setRotation(0);
//        
//        camera.setParameters(parameters);
        
        //setCameraDisplayOrientation( activity , info , camera );
        
        Log.d( tag , " ########################### " ) ;
        
        camera.release() ;
        
        return null;
    }
    
    /**
     * @private
     */
    private String tag = "Camera::Initialize"; 
    
//    private void setCameraDisplayOrientation( Activity activity, CameraInfo info , Camera camera) 
//    {
//        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation() ;
//        int degrees  = 0 ;
//        
//        Log.d( tag , " setCameraDisplayOrientation display rotation  : " + rotation ) ;
//        
//        switch (rotation) 
//        {
//            case Surface.ROTATION_0   : degrees = 0   ; break;
//            case Surface.ROTATION_90  : degrees = 90  ; break;
//            case Surface.ROTATION_180 : degrees = 180 ; break;
//            case Surface.ROTATION_270 : degrees = 270 ; break;
//        }
//        
//        Log.i( tag , " setCameraDisplayOrientation degrees  : " + degrees ) ;
//        
//        int angle = 0 ;
//        
//        if ( info.facing == CameraInfo.CAMERA_FACING_FRONT) 
//        {
//            angle = (info.orientation + degrees) % 360;
//            angle = (360 - angle) % 360;  // compensate the mirror
//        }
//        else // back-facing
//        {
//            angle = (info.orientation - degrees + 360) % 360;
//        }
//        
//        Log.i( tag , " setCameraDisplayOrientation angle  : " + angle ) ;
//        
//        setDisplayOrientation( camera , 90 ) ;
//    }
//    
//    /**
//     * @private
//     */
//    private void setDisplayOrientation( Camera camera, int angle )
//    {
//        try
//        {
//            Method downPolymorphic ;
//            downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
//            if ( downPolymorphic != null )
//            {
//                downPolymorphic.invoke(camera, new Object[] { angle });
//            }
//        }
//        catch ( Exception e )
//        {
//            e.printStackTrace() ;
//        }
//    }
}
