/*
 
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at
 
 http://www.mozilla.org/MPL/
 
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the License.
 
 The Original Code is NinjAS Framework.
 
 The Initial Developer of the Original Code is
 ALCARAZ Marc <ekameleon@gmail.com>.
 Portions created by the Initial Developer are Copyright (C) 2009-2011
 the Initial Developer. All Rights Reserved.
 
 Contributor(s) :
 
 Alternatively, the contents of this file may be used under the terms of
 either the GNU General Public License Version 2 or later (the "GPL"), or
 the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the LGPL or the GPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.
 
 */

#import "FlashRuntimeExtensions.h"
#import <UIKit/UIKit.h>

#define DEFINE_ANE_FUNCTION(fn) FREObject (fn)(FREContext context, void* functionData, uint32_t argc, FREObject argv[])

// functions

DEFINE_ANE_FUNCTION( exist )
{
    uint32_t length = 0;
    
    const uint8_t* urlParam = NULL;
    
    if( FREGetObjectAsUTF8( argv[0], &length, &urlParam ) != FRE_OK )
    {
        return NULL;
    }
    
    NSString* url = [NSString stringWithUTF8String: (char*) urlParam];
    
    Boolean canOpen = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]];
    
    FREObject result;
    
    if( FRENewObjectFromBool( canOpen, &result ) == FRE_OK )
    {
        return result;
    }
    
    return NULL;
}

// context

void CheckUrlContextInitializer( void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctionsToSet, const FRENamedFunction** functionsToSet )
{
	*numFunctionsToSet = 1;
    
	FRENamedFunction* func = (FRENamedFunction*) malloc( sizeof(FRENamedFunction) );
    
	func[0].name         = (const uint8_t*) "exist" ;
	func[0].functionData = NULL ;
    func[0].function     = &exist ;
    
	*functionsToSet = func ;
}

void CheckUrlContextFinalizer( FREContext ctx )
{
	return;
}

// extension

void CheckUrlExtensionInitializer( void** extDataToSet, FREContextInitializer* ctxInitializerToSet, FREContextFinalizer* ctxFinalizerToSet )
{
    extDataToSet         = NULL ; // Does not use any extension data.
    *ctxInitializerToSet = &CheckUrlContextInitializer;
    *ctxFinalizerToSet   = &CheckUrlContextFinalizer;
}

void CheckUrlExtensionFinalizer()
{
    return;
}
