﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.content 
{
    import flash.external.ExtensionContext;
    
    /**
     * This extension check if a specified url exist.
     */
    public class CheckUrl 
    {
        /**
         * Creates a new CheckUrl instance.
         */
        public function CheckUrl()
        {
            _context = ExtensionContext.createExtensionContext( EXTENSION_ID , null ) ;
        }
        
        /**
         * The id of the extension.
         */
        public static const EXTENSION_ID:String = "ninjas.extensions.content.CheckUrl" ;
        
        /**
         * Indicates if the native extension is supported in the application.
         */
        public function get available():Boolean
        {
            return _context != null ;
        }
        
        /**
         * Dispose the extension.
         */
        public function dispose():void
        {
            _context = null ;
        }
        
        /**
         * Indicates if the specified url exist.
         */
        public function exist( url:String = "" ):Boolean
        {
            return ( _context != null ) ? _context.call( "exist" , url ) as Boolean : false ; 
        }
        
        /**
         * @private
         */
        private var _context:ExtensionContext ;
    }
    
}
