/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2014
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.extensions.alert.functions;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.os.Build;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import ninjas.extensions.alert.AlertExtension ;

public class Notify implements FREFunction
{
    @Override
    public FREObject call( FREContext context , FREObject[] params )
    {
        String title   = null   ;
        String message = null   ;
        String first   = null   ;
        String second  = null   ;
        String theme   = null   ;
        
        try
        {
            title   = params[0].getAsString();
            message = params[1].getAsString();
            
            first = params[2].getAsString();
            
            if ( params[3] != null ) 
            {
                  second = params[3].getAsString() ;
            }

            if ( params[4] != null ) 
            {
                  theme = params[4].getAsString() ;
            }
        }
        catch ( Exception e )
        {
              e.printStackTrace();
        }
        
        AlertDialog.Builder builder;
        
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH )
        {
            builder = new AlertDialog.Builder( context.getActivity() , (theme == "light") ? AlertDialog.THEME_DEVICE_DEFAULT_LIGHT : AlertDialog.THEME_DEVICE_DEFAULT_DARK ) ;
        }
        else if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB )
        {
            builder = new AlertDialog.Builder( context.getActivity(), (theme == "light") ? AlertDialog.THEME_HOLO_LIGHT : AlertDialog.THEME_HOLO_DARK ) ;
        }
        else
        {
            builder = new AlertDialog.Builder( context.getActivity() ) ;
        }
        
        builder.setTitle( title ).setMessage( message ).setPositiveButton( first , AlertExtension.context ).setOnCancelListener( AlertExtension.context ) ;
        
        if ( second != null )
        {
            builder.setNeutralButton( second , AlertExtension.context ) ;
        }
        
        AlertDialog dialog = builder.create();
        
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        
        return null;
    }
}
