﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-214
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.alert 
{
    import flash.external.ExtensionContext;
    
    import flash.events.StatusEvent;
    
    /**
     * This extension display an alert message.
     */
    public class Alert
    {
        /**
         * Creates a new Alert instance.
         */
        public function Alert()
        {
            _context = ExtensionContext.createExtensionContext( EXTENSION_ID , null ) ;
            
            _context.addEventListener( StatusEvent.STATUS , _status ) ;
        }
        
        /**
         * Indicates if the native extension is supported in the application.
         */
        public function get available():Boolean
        {
            return _context != null ;
        }
        
        
        /**
         * The id of the extension.
         */
        public static const EXTENSION_ID:String = "ninjas.extensions.alert.Alert" ;
        
        /**
         * Invoked if a button is clicked.
         */
        public var onClick:Function ;
        
        /**
         * Invoked if the alert is canceled.
         */
        public var canceled:Function ;
        
        /**
         * Invoked if the first button is clicked.
         */
        public var first:Function ;
        
        /**
         * Invoked if the second button is clicked.
         */
        public var second:Function ;
        
        /**
         * Indicates the theme of the alert ("dark" or "light").
         */
        public function get theme():String 
        {
            return _theme ;
        }
        
        /**
         * @private
         */
        public function set theme( value:String ):void
        {
            _theme = value == THEME_DARK ? THEME_DARK : THEME_LIGHT ;
        }
        
        /**
         * The "dark" theme value.
         */
        public static const THEME_DARK:String = "dark" ;
        
        /**
         * The "light" theme value.
         */
        public static const THEME_LIGHT:String = "light" ;
        
        /**
         * Dispose the extension.
         */
        public function dispose():void
        {
            _context = null ;
        }
        
        /**
         * Notify a message.
         * @param title The title of the message
         * @param message The message to notify
         * @param firstLabel The first button label.
         * @param secondLabel The second button label.
         */
        public function notify( title:String = "" , message:String = "" , firstLabel:String = "OK" , secondLabel:String = "CANCEL" ):void
        {
            if( _context )
            {
                _context.call( "notify" , title , message, firstLabel , secondLabel , _theme ) ; 
            }
        }
        
        /**
         * @private
         */
        private var _context:ExtensionContext ;
        
        /**
         * @private
         */
        private var _theme:String = "light" ;
        
        /**
         * @private
         */
        private function _status( event:StatusEvent ):void
        {
            if ( event.code == "CLICK" )
            {
                if( onClick is Function )
                {
                    onClick( event.level ) ;
                }
                
                switch ( event.level ) 
                {
                    case "1" :
                    {
                        if( first is Function )
                        {
                          first() ;
                        }
                        break ;
                    }
                    case "0" :
                    {
                        if( second is Function )
                        {
                          second() ;
                        }
                        break ;
                    }
                    case "-1" :
                    {
                        if( canceled is Function )
                        {
                            canceled() ;
                        }
                        break ;
                    }
                }
            }
        }
    }
}
