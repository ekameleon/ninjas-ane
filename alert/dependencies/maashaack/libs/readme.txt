Maashaack Opensource Framework - revision 9627

LICENCE
 
    Version: MPL 1.1/GPL 2.0/LGPL 2.1
    
        > Licence MPL 1.1  http://www.mozilla.org/MPL/MPL-1.1.html
        > Licence GPL 2    http://www.gnu.org/licenses/gpl-2.0.html
        > Licence LGPL 2.1 http://www.gnu.org/licenses/lgpl-2.1.html
   
PROJECT PAGES

    http://code.google.com/p/maashaack/
    
ABOUT AUTHOR
    
    Author : ALCARAZ Marc (eKameleon)
    Mail   : ekameleon@gmail.com
