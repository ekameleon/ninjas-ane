﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-214
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.alert 
{
    /**
     * This extension display an alert message.
     */
    public class Alert
    {
        /**
         * Creates a new Alert instance.
         */
        public function Alert()
        {
            //
        }
        
        /**
         * Indicates if the native extension is supported in the application.
         */
        public function get available():Boolean
        {
            return false ;
        }
        
        /**
         * Invoked if the first button is clicked.
         */
        public var first:Function ;

        /**
         * Invoked if the second button is clicked.
         */
        public var second:Function ;
        
        /**
         * Indicates the theme of the alert ("dark" or "light").
         */
        public function get theme():String 
        {
            return _theme ;
        }
        
        /**
         * @private
         */
        public function set theme( value:String ):void
        {
            _theme = value == THEME_DARK ? THEME_DARK : THEME_LIGHT ;
        }
        
        /**
         * The "dark" theme value.
         */
        public static const THEME_DARK:String = "dark" ;
        
        /**
         * The "light" theme value.
         */
        public static const THEME_LIGHT:String = "light" ;
        
        /**
         * Dispose the extension.
         */
        public function dispose():void
        {
            //
        }
        
        /**
         * Notify a message.
         */
        public function notify( title:String = "" , message:String = "" , firstLabel:String = "OK" , secondLabel:String = "CANCEL" ):void
        {
            //
        }
        
        /**
         * @private
         */
        private var _theme:String = "light" ;
    }
}
