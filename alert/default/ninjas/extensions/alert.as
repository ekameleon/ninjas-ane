﻿package ninjas.extensions
{
    import ninjas.extensions.alert.Alert;
    
    /**
     * The alert extension singleton
     */
    public const alert:Alert = new Alert() ;
}
