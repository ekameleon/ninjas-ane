/*
 
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at
 
 http://www.mozilla.org/MPL/
 
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the License.
 
 The Original Code is NinjAS Framework.
 
 The Initial Developer of the Original Code is
 ALCARAZ Marc <ekameleon@gmail.com>.
 Portions created by the Initial Developer are Copyright (C) 2009-2011
 the Initial Developer. All Rights Reserved.
 
 Contributor(s) :
 
 Alternatively, the contents of this file may be used under the terms of
 either the GNU General Public License Version 2 or later (the "GPL"), or
 the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the LGPL or the GPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.
 
 */

#import "Alert.h"

FREContext AlertCtx = nil ;

@interface Alert ()

@property (nonatomic, strong) UIAlertView *alertView;

@end

@implementation Alert

#pragma mark - Singleton

static Alert *sharedInstance = nil;

+ (Alert *)sharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}

- (id)copy
{
    return self;
}

#pragma mark - NSObject

- (void)dealloc
{
    self.alertView = nil;
    [super dealloc];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    FREDispatchStatusEventAsync( AlertCtx , (const uint8_t *)"CLICK", (const uint8_t *)[[NSString stringWithFormat:@"%u", buttonIndex] UTF8String]) ;
}

@end


#pragma mark - C interface

DEFINE_ANE_FUNCTION( AlertNotify )
{
    uint32_t stringLength;
    
    NSString *title   = nil ;
    NSString *message = nil ;
    NSString *button1 = nil ;
    NSString *button2 = nil ;
    NSString *theme   = nil ;
    
    // Retrieve title
    const uint8_t *titleString;
    
    if (FREGetObjectAsUTF8(argv[0], &stringLength, &titleString) == FRE_OK)
    {
        title = [NSString stringWithUTF8String:(char *)titleString];
    }
    
    // Retrieve message
    
    const uint8_t *messageString;
    if (FREGetObjectAsUTF8(argv[1], &stringLength, &messageString) == FRE_OK)
    {
        message = [NSString stringWithUTF8String:(char *)messageString];
    }
    
    // Retrieve button 1
    
    const uint8_t *button1String;
    if (FREGetObjectAsUTF8(argv[2], &stringLength, &button1String) == FRE_OK)
    {
        button1 = [NSString stringWithUTF8String:(char *)button1String];
    }
    
    // Retrieve button 2
    
    if ( argc > 3 && argv[3] )
    {
        const uint8_t *button2String;
        if (FREGetObjectAsUTF8(argv[3], &stringLength, &button2String) == FRE_OK)
        {
            button2 = [NSString stringWithUTF8String:(char *)button2String];
        }
    }

    if ( argc > 4 && argv[4] )
    {
        const uint8_t *themeString;
        if (FREGetObjectAsUTF8(argv[4], &stringLength, &themeString) == FRE_OK)
        {
            theme = [NSString stringWithUTF8String:(char *)themeString];
        }
    }
    
    // Setup and show the alert
    __block UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:[Alert sharedInstance] cancelButtonTitle:button2 otherButtonTitles:nil];

    [alertView addButtonWithTitle:button1] ;
   
    [[Alert sharedInstance] setAlertView:alertView];
    [alertView release];
    
    // We had cases of deadlock when calling directly [alertView show] on iOS 5.
    // When this happens, the app freezes completely without crashing, and the popup is not displayed.
    //
    // Explicitely calling [alertView show] on the main thread fixes the issue. ANE functions are
    // supposed to be called on the main thread so we shouldn't need to do this... But hey, it works!
    //
    // Tested on an iPod Touch (2010) on iOS 5.1.1 with AIR 3.6, AIR 3.7 and AIR 3.8b.
    //
    // Console logs:
    //      <Warning>: *** -[NSLock lock]: deadlock (<NSLock: 0xd5d420> '(null)')
    //      <Warning>: *** Break on _NSLockError() to debug.
    dispatch_async( dispatch_get_main_queue(), ^{ [alertView show]; });
    
    return nil;
}


#pragma mark - ANE setup

void AlertContextInitializer( void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctionsToTest, const FRENamedFunction** functionsToSet )
{
    // Register the links btwn AS3 and ObjC. (dont forget to modify the nbFuntionsToLink integer if you are adding/removing functions)
    
    NSInteger nbFuntionsToLink = 1;
    
    *numFunctionsToTest = nbFuntionsToLink;
    
    FRENamedFunction* func = (FRENamedFunction*) malloc(sizeof(FRENamedFunction) * nbFuntionsToLink);
    
    func[0].name         = (const uint8_t*) "notify" ;
    func[0].functionData = NULL ;
    func[0].function     = &AlertNotify ;
    
    *functionsToSet = func ;
    
    AlertCtx = ctx ;
}

void AlertContextFinalizer(FREContext ctx)
{
    // do nothing
}

void AlertExtensionInitializer(void** extDataToSet, FREContextInitializer* ctxInitializerToSet, FREContextFinalizer* ctxFinalizerToSet)
{
	*extDataToSet         = NULL ;
	*ctxInitializerToSet  = &AlertContextInitializer ;
	*ctxFinalizerToSet    = &AlertContextFinalizer   ;
}

void AlertExtensionFinalizer(void* extData)
{
    // do nothing
}