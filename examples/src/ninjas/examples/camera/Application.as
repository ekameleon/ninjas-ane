﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.examples.camera
{
    import core.reflect.getClassPackage;
    import core.dump;

    import graphics.display.MOB;
    import graphics.screens.orientation;

    import ninjas.examples.logging.logger;
    import ninjas.examples.ui.AndroidKeyDown;
    import ninjas.extensions.camera.CameraInfo;
    import ninjas.extensions.camera.CameraManager;

    import system.eden;
    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;
    import system.logging.targets.TraceTarget;

    import flash.display.StageAlign;
    import flash.display.StageAspectRatio;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.events.StageOrientationEvent;
    import flash.events.UncaughtErrorEvent;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.media.Camera;
    import flash.media.Video;
    
    [SWF(width="480", height="800", frameRate="24", backgroundColor="#666666")]
    
    /**
     * The camera example application to use native extensions.
     */
    public class Application extends MOB
    {
        public function Application()
        {
            /////// Global Error
            
            loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtError );
            
            ////// Initialize
            
            var ip:String   = "localhost" ;
            var name:String = "desktop application" ;
            var version:String = "0.0.0.0" ;
            
            /*FDT_IGNORE*/
            ip      = CONFIG::ip ;
            name    = CONFIG::name ;
            version = CONFIG::version ;
            /*FDT_IGNORE*/
            
            try
            {
                var pkg:String = getClassPackage( this ) ;
                var file:File =  new File( File.documentsDirectory.nativePath + File.separator + pkg + ".cfg" ) ;
                if( file && file.exists )
                {
                    var stream:FileStream = new FileStream();
                    
                    stream.open( file , FileMode.READ ) ;
                    
                    var exp:String = stream.readUTFBytes( file.size ) ;
                    
                    var cfg:Object = eden.deserialize( exp ) ;
                    
                    if ( cfg["ip"] != undefined )
                    {
                        ip = cfg["ip"] ;
                    }
                    
                    stream.close();
                }
            }
            catch( er:Error )
            {
                //
            }
           
            var sos:SOSTarget = new SOSTarget( name , 0xA2A2A2 , true , ip ) ;
            
            sos.filters       = ["*"] ;
            sos.level         = LoggerLevel.ALL ;
            sos.includeLines  = true  ;
            sos.includeTime   = true  ;
            
            var tracer:TraceTarget = new TraceTarget() ;
            
            tracer.filters       = ["*"] ;
            tracer.level         = LoggerLevel.ALL ;
            tracer.includeLines  = true  ;
            tracer.includeTime   = true  ;
            
            logger.info( "## " + this + " version     : " + version ) ; 
            logger.info( "## " + this + " ip          : " + ip ) ;
            
            /////// stage
            
            stage.align     = StageAlign.TOP_LEFT ;
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            /////// Handle events
            
            loaderInfo.addEventListener( Event.COMPLETE , complete );
        }
        
        //////////////
        
        /**
         * @private
         */
        protected var info:CameraInfo ;
        
        protected function down( e:Event ):void
        {
            logger.debug( "## " + this + " down." ) ;
            stage.setAspectRatio( StageAspectRatio.LANDSCAPE ) ;
            logger.info( "## " + this + " down orientation : " + stage.orientation ) ;
            logger.info( this + " stageWidth       : " + stage.stageWidth ) ;
            logger.info( this + " stageHeight      : " + stage.stageHeight ) ;
            logger.info( this + " fullScreenWidth  : " + stage.fullScreenWidth ) ;
            logger.info( this + " fullScreenHeight : " + stage.fullScreenHeight ) ;
        }
        
        protected function resize( e:Event ):void
        {
            build() ;
            logger.log( "## " + this + " resize." ) ;
            logger.info( this + " stageWidth       : " + stage.stageWidth ) ;
            logger.info( this + " stageHeight      : " + stage.stageHeight ) ;
            logger.info( this + " fullScreenWidth  : " + stage.fullScreenWidth ) ;
            logger.info( this + " fullScreenHeight : " + stage.fullScreenHeight ) ;
        }
        
        protected function orientationChange( e:StageOrientationEvent ):void
        {
            logger.log( this + " -----------" ) ;
            logger.debug( "## " + this + " orientationChange type:" + e.type + " afterOrientation:" + e.afterOrientation + " beforeOrientation:" + e.afterOrientation) ;
            logger.info(  "## " + this + " fullScreenWidth  : " + stage.fullScreenWidth ) ;
            logger.info(  "## " + this + " fullScreenHeight : " + stage.fullScreenHeight ) ;
        }
        
        /**
         * Invoked when the application loading is complete.
         */
        protected function complete( e:Event = null ):void
        {
            logger.debug( "## " + this + " complete." ) ;
            
            logger.log( this + " -----------" ) ;
            
            if( stage )
            {
                stage.addEventListener( KeyboardEvent.KEY_DOWN , (new AndroidKeyDown()).handleEvent ) ;
                stage.addEventListener( MouseEvent.MOUSE_DOWN , down ) ;
                stage.addEventListener( Event.RESIZE , resize ) ;
                
                logger.info( "## " + this + " device      : " + stage.deviceOrientation ) ;
                logger.info( "## " + this + " orientation : " + stage.orientation ) ;
                logger.info( "## " + this + " screen orientation : " + orientation ) ;
                
                logger.info( this + " stageWidth       : " + stage.stageWidth ) ;
                logger.info( this + " stageHeight      : " + stage.stageHeight ) ;
                logger.info( this + " fullScreenWidth  : " + stage.fullScreenWidth ) ;
                logger.info( this + " fullScreenHeight : " + stage.fullScreenHeight ) ;
                
                stage.addEventListener( StageOrientationEvent.ORIENTATION_CHANGE , orientationChange ) ;
            }
            
            try
            {
                var cameras:Array = Camera.names ;
                
                logger.info( this + " cameras : " + dump( cameras ) ) ;
                
                var manager:CameraManager = new CameraManager() ;
                
                logger.debug( this + " manager :" + manager ) ;
                
                if( manager )
                {
                    for each( var name:String in cameras )
                    {
                        info = manager.getCameraInfo( int(name) ) ;
                        logger.info( this + " camera:'" + name + "' facing back:" + (info.facing == CameraInfo.CAMERA_FACING_BACK) + " orientation:" + info.orientation ) ;
                    }
                }
                else
                {
                    logger.warn( "## extension " + CameraManager.EXTENSION_ID + " is not available." ) ;
                }
            }
            catch( er:Error )
            {
                logger.error( "## failed to create the context of the extension " + CameraManager.EXTENSION_ID + " message: " + er.message + " er:" + er.toString() ) ;
            }
            
            build() ;
        }
        
        protected var camera:Camera ;
        
        protected var video:Video ;
        
        protected function build():void
        {
            dispose() ;
            
            camera = Camera.getCamera()  ;
            
            video = new Video() ;
            
            video.attachCamera( camera ) ;
            
            video.x = 0 ;
            video.y = 0 ;
            
            var size:Number =  Math.min( stage.fullScreenWidth , stage.fullScreenHeight ) ;
            
            camera.setMode( size , size , 12 ) ;
            
            video.width  = size ;
            video.height = size ;
            
            addChild( video ) ;
        }
        
        protected function dispose():void
        {
            if( camera )
            {
                camera = null ;
            }
            
            if( video )
            {
                video.attachCamera( null ) ;
                video.clear() ;
                if( contains( video ) )
                {
                    removeChild( video ) ;
                }
                video = null ;
            }
            
        }
        
//        
//        public function start():void
//        {
//            if( !hasEventListener( Event.ENTER_FRAME ) )
//            {
//                addEventListener( Event.ENTER_FRAME , loop ) ;
//            }
//        }
//        
//        public function stop():void
//        {
//            if( hasEventListener( Event.ENTER_FRAME ) )
//            {
//                removeEventListener( Event.ENTER_FRAME , loop ) ;
//            }
//        }
//        
//        protected var bitmap:BitmapData ;
//        
//        protected var decoder:QRCode ;
        
//        
//        protected function initialize():void
//        {
//            try
//            {
//                var manager:CameraManager = new CameraManager() ;
//                
//                logger.debug( this + " manager :" + manager ) ;
//                
//                if( manager )
//                {
//                    info = manager.getCameraInfo() ;
//                    
//                    logger.info( this + " orientation : " + orientation ) ;
//                }
//                else
//                {
//                    logger.warn( "## extension " + CameraManager.EXTENSION_ID + " is not available." ) ;
//                }
//                
////                decoder = new QRCode() ;
//            }
//            catch( er:Error )
//            {
//                logger.error( "## failed to create the context of the extension " + CameraManager.EXTENSION_ID + " message: " + er.message + " er:" + er.toString() ) ;
//            }
//        }
//        
//        protected function loop( e:Event = null ):void
//        {
//            if( decoder )
//            {
//                bitmap.draw( video ) ;
//                var result:String = decoder.decode( bitmap ) ;
//                if( result != null && result.length > 0 )
//                {
//                    logger.info( this + " decode : " + result ) ;
//                    stop() ;
//                }
//                else
//                {
//                    logger.warn(this + " decoder in progress : " + result ) ;
//                }
//            }
//        }
        
        ////////////// Global error handler
        
        protected function uncaughtError( e:UncaughtErrorEvent ):void
        {
            logger.error( "### error:" + e.error + " id:" + e.errorID ) ;
        }
    }
}
