﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.examples.bug
{
    import ninjas.examples.logging.logger;
    
    import flash.display.Sprite;
    import flash.display.StageAspectRatio;
    import flash.events.UncaughtErrorEvent;
    
    [SWF(width="480", height="800", frameRate="24", backgroundColor="#666666")]
    
    public class Application extends Sprite
    {
        public function Application()
        {
            loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtError );
            
            if( stage )
            {
                var square:Sprite = new Sprite() ;
                
                square.graphics.beginFill(0xFF0000);
                square.graphics.drawRect(0,0,100,100) ;
                
                stage.scaleMode = "noScale" ;
                stage.align = "tl" ;
                
                stage.addChild( square ) ;
                 
                stage.setAspectRatio( StageAspectRatio.LANDSCAPE ) ;
            }
        }
        
        ////////////// Global error handler
        
        protected function uncaughtError( e:UncaughtErrorEvent ):void
        {
            logger.error( "### error:" + e.error + " id:" + e.errorID ) ;
        }
    }
}
