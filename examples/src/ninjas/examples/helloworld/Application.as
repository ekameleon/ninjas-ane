﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.examples.helloworld
{
    import graphics.display.MOB;

    import ninjas.examples.logging.logger;
    import ninjas.extensions.helloworld.HelloWorld;

    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;
    import system.logging.targets.TraceTarget;

    import flash.events.Event;
    import flash.events.UncaughtErrorEvent;
    
    [SWF(width="480", height="800", frameRate="24", backgroundColor="#666666")]
    
    /**
     * The basic helloworld application to use native extensions.
     */
    public class Application extends MOB
    {
        public function Application()
        {
            var ip:String      = "localhost" ;
            var name:String    = "desktop application" ;
            var version:String = "0.0.0.0" ;
            
            /*FDT_IGNORE*/
            ip      = CONFIG::ip ;
            name    = CONFIG::name ;
            version = CONFIG::version ;
            /*FDT_IGNORE*/
            
            var sos:SOSTarget = new SOSTarget( name , 0xA2A2A2 , true , ip ) ;
            
            sos.filters       = ["*"] ;
            sos.level         = LoggerLevel.ALL ;
            sos.includeLines  = true  ;
            sos.includeTime   = true  ;
            
            var tracer:TraceTarget = new TraceTarget() ;
            
            tracer.filters       = ["*"] ;
            tracer.level         = LoggerLevel.ALL ;
            tracer.includeLines  = true  ;
            tracer.includeTime   = true  ;
            
            logger.info( "## " + this + " version : " + version + " - debug with ip : " + ip ) ;
            
            /////// Handle events
            
            loaderInfo.addEventListener( Event.COMPLETE , complete );
            
            /////// Global Error
            
            loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtError );
        }
        
        //////////////
        
        /**
         * Invoked when the application loading is complete.
         */
        protected function complete( e:Event = null ):void
        {
            logger.debug( "## " + this + " complete." ) ;
            
            logger.log( this + " -----------" ) ;
            
            try
            {
                var hello:HelloWorld = new HelloWorld() ;
                
                logger.debug( this + " helloworld context : " + hello ) ;
                
                if( hello )
                {
                    var message:String = hello.notify("Shikamaru") ;
                    logger.info( this + " message : " + message ) ;
                }
                else
                {
                    logger.warn( "## extension " + HelloWorld.EXTENSION_ID + " is not available." ) ;
                }
            }
            catch( er:Error )
            {
                logger.error( "## failed to create the context of the extension " + HelloWorld.EXTENSION_ID + " message: " + er.message + " er:" + er.toString() ) ;
            }
        }
        
        ////////////// Global error handler
        
        protected function uncaughtError( e:UncaughtErrorEvent ):void
        {
            logger.error( "### error:" + e.error + " id:" + e.errorID ) ;
        }
    }
}
