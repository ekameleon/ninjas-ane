﻿package ninjas.examples.ui
{
    import ninjas.examples.logging.logger;

    import system.events.EventListener;

    import flash.desktop.NativeApplication;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    /**
     * Invoked to handle the Android system keys.
     */
    public class AndroidKeyDown implements EventListener
    {
        /**
         * Creates a new AndroidKeyDown instance.
         */
        public function AndroidKeyDown()
        {
            //
        }
        
        /**
         * Handles the event.
         */
        public function handleEvent( e:Event ):void
        {
            var event:KeyboardEvent = e as KeyboardEvent ;
            if( event )
            {
                logger.debug( this + " handleEvent." ) ;
                
                var code:uint = event.keyCode ;
                switch( code )
                {
                    case Keyboard.BACK :
                    {
                        logger.info( this + " Back Pressed." ) ;
                        if( NativeApplication.nativeApplication )
                        {
                            NativeApplication.nativeApplication.exit();
                        }
                        break ;
                    }
                    case Keyboard.MENU :
                    {
                        logger.info( this + " Menu Pressed." ) ;
                        break ;
                    }
                    case Keyboard.SEARCH :
                    {
                        logger.info( this + " Search Pressed." ) ;
                        break ;
                    }
                }
            }
            
            event.preventDefault();
        }
    }
}
