/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.qrcode;

import java.util.HashMap;
import java.util.Map;

import ninjas.extensions.qrcode.functions.Decode;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.google.zxing.Reader;
import com.google.zxing.qrcode.QRCodeReader;

public class QRCodeContext extends FREContext
{
    /**
     * Creates a new QRCodeContext instance.
     */
    public QRCodeContext()
    {
        Log.i( tag , "Creating QRCodeContext context." ); 
    }
    
    /**
     * The reader reference.
     */
    public Reader reader ;  
    
    @Override
    public void dispose()
    {
        Log.i( tag , "QRCode.dispose." ) ;
        availableFunctions = null ;
        reader             = null ;
    }
    
    @Override
    public Map<String, FREFunction> getFunctions()
    {
        reader = new QRCodeReader() ;
        
        Log.i( tag , "QRCodeContext.getFunctions, creating function Map." ) ; 
        
        availableFunctions = new HashMap<String, FREFunction>();
        
        availableFunctions.put( "decode" , new Decode() );
        
        return availableFunctions;
    }
    
    /**
     * The available functions of this context.
     */
    private HashMap<String, FREFunction> availableFunctions;
    
    /**
     * @private
     */
    private String tag = "NinjAS::QRCode";
}
