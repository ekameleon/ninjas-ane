/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.extensions.qrcode.functions;

import ninjas.extensions.qrcode.QRCodeContext;
import ninjas.extensions.qrcode.display.BitmapDataLuminance;
import android.graphics.Bitmap;
import android.util.Log;

import com.adobe.fre.FREBitmapData;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

public class Decode implements FREFunction
{
    @Override
    public FREObject call( FREContext context , FREObject[] args )
    {
        FREObject text = null ;
        
        Log.d( tag , "Decode is called." );
        
        if( args.length == 1 ) 
        {
            try 
            {
                Bitmap bmp           = null ;
                QRCodeContext ctx    = (QRCodeContext) context ;
                FREBitmapData image  = (FREBitmapData) args[0] ;
                
                /////////////////
                
                image.acquire() ;
                
                bmp = Bitmap.createBitmap( image.getWidth(), image.getHeight() , Bitmap.Config.ARGB_4444 ) ;
                bmp.copyPixelsFromBuffer( image.getBits() ) ;
                
                image.release() ;
                
                /////////////////
                
                LuminanceSource source = new BitmapDataLuminance( bmp );
                
                BinaryBitmap bitmap = new BinaryBitmap( new HybridBinarizer( source ) ) ;  
                
                Result result = ctx.reader.decode( bitmap );  
                
                Log.i( tag , "Decode text : " + result.getText() );
                
                text = FREObject.newObject( result.getText() ) ;
            } 
            catch ( Throwable e ) 
            {
                e.printStackTrace() ;
            }
        }
        else
        {
            Log.w( tag , "Decode failed, the bitmap reference not must be null. " ); 
        }
        
        return text ;
    }
    
    /**
     * @private
     */
    private String tag = "NinjAS::QRCode"; 
}
