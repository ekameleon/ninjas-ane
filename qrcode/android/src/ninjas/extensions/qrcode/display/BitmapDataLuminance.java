package ninjas.extensions.qrcode.display;

import android.graphics.Bitmap;

import com.google.zxing.LuminanceSource;

/**
 * This class is used to help decode images from files which arrive as RGB data from Android bitmaps. 
 * It does not support cropping or rotation.
 */
public class BitmapDataLuminance extends LuminanceSource
{
    /**
     * Creates a new BitmapDataLuminance instance.
     * @param bitmap The Bitmap reference to decode.
     */
    public BitmapDataLuminance( Bitmap bitmap )
    {
        super( bitmap.getWidth(), bitmap.getHeight() );
        
        int w = bitmap.getWidth()  ;
        int h = bitmap.getHeight() ;
        
        int[] pixels = new int[w * h] ;
        
        bitmap.getPixels( pixels , 0 , w , 0 , 0 , w , h ) ;
        
        luminances = new byte[w * h];
        
        for (int y = 0; y < h; y++) 
        {
            int offset = y * w ;
            for ( int x = 0 ; x < w ; x++ ) 
            {
                int pixel = pixels[ offset + x ] ;
                
                int r = (pixel >> 16) & 0xFF ;
                int g = (pixel >> 8) & 0xFF ;
                int b = pixel & 0xFF ;
                
                if ( r == g && g == b )  
                {
                    luminances[offset + x] = (byte) r ; // Image is greyscale, so pick any channel.
                } 
                else
                {
                    luminances[offset + x] = (byte) ( (r + g + g + b ) >> 2 ) ; // Calculates luminance cheaply, favoring green.
                }
            }
        }
    }
    
    @Override
    public byte[] getRow(int y, byte[] row)
    {
        if (y < 0 || y >= getHeight())
        {
            throw new IllegalArgumentException( "Requested row is outside the image: " + y ) ;
        }
        
        int width = getWidth() ;
        
        if (row == null || row.length < width)
        {
            row = new byte[width];
        }
        
        System.arraycopy(luminances, y * width, row, 0, width) ;
        return row;
    }
    
    @Override
    public byte[] getMatrix()
    {
        return luminances ;
    }
    
    /**
     * @private
     */
    private final byte[] luminances;
}
