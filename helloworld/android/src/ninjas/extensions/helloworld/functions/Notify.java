/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.extensions.helloworld.functions;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

public class Notify implements FREFunction
{
    @Override
    public FREObject call( FREContext context , FREObject[] params )
    {
        FREObject result = null ;
        
        try
        {
            String prefix = "hello " ;
            String name   = getString( params[0] );
            
            StringBuffer dest = new StringBuffer( prefix.length() + name.length() );
            
            dest.append( "hello " ) ;
            dest.append( name     ) ;
            
            result = FREObject.newObject( dest.toString() );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        
        Log.i( tag , "call : " + result.toString() );
        
        return result ;
    }
    
    protected int getInt( FREObject value )
    {
        int i = 1;
        try
        {
            i = value.getAsInt();
        }
        catch ( Exception e )
        {
            e.printStackTrace() ;
        }
        return i;
    }
    
    protected String getString( FREObject value )
    {
        String str = null ;
        try
        {
            str = value.getAsString();
        } 
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return str ;
    }
    
    
    /**
     * @private
     */
    private String tag = "HelloWorld::Function::Notify"; 
}
